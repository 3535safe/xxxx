function cyrb53(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed,
      h2 = 0x41c6ce57 ^ seed;
    for (let i = 0, ch; i < str.length; i++) {
      ch = str.charCodeAt(i);
      h1 = Math.imul(h1 ^ ch, 2654435761);
      h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    
    h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909);
    h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909);
    
    return 4294967296 * (2097151 & h2) + (h1 >>> 0);
  };

  function randomX(seed) {
    var x = Math.sin(seed++) * 10000; 
    return x - Math.floor(x);
  }
  
  function shuffleX(array, seed) {
    var m = array.length, t, i;
    while (m) {
      i = Math.floor(randomX(seed) * m--);
      t = array[m];
      array[m] = array[i];
      array[i] = t;
      ++seed;
    }
    return array;
  }
  
  const iosDevices = [
      { name: "12.9 iPad Pro", width: 2048, height: 2732, safe_t: 24, safe_b: 20 },
      { name: "11 iPad Pro", width: 1668, height: 2388, safe_t: 24, safe_b: 20 },
      { name: "10.2 iPad", width: 1620, height: 2160, safe_t: 20, safe_b: 0 },
      { name: "8.3 iPad mini", width: 1488, height: 2266, safe_t: 24, safe_b: 20 },
      { name: "iPhone 14 Pro Max", width: 1290, height: 2796, safe_t: 59, safe_b: 34 },
      { name: "iPhone 14 Pro", width: 1179, height: 2556, safe_t: 59, safe_b: 34 },
      { name: "iPhone 14 Plus", width: 1284, height: 2778, safe_t: 47, safe_b: 34 },
      { name: "iPhone 14", width: 1170, height: 2532, safe_t: 47, safe_b: 34 },
      { name: "iPhone 13 Pro Max", width: 1284, height: 2778, safe_t: 47, safe_b: 34 },
      { name: "iPhone 13 Pro", width: 1170, height: 2532, safe_t: 47, safe_b: 34 },
      { name: "iPhone 13", width: 1170, height: 2532, safe_t: 47, safe_b: 34 },
      { name: "iPhone 13 mini", width: 1125, height: 2436, safe_t: 50, safe_b: 34 },
      { name: "iPhone 12 Pro Max", width: 1284, height: 2778, safe_t: 47, safe_b: 19 },
      { name: "iPhone 12 Pro", width: 1170, height: 2532, safe_t: 47, safe_b: 19 },
      { name: "iPhone 12", width: 1170, height: 2532, safe_t: 47, safe_b: 19 },
      { name: "iPhone 12 mini", width: 1125, height: 2436, safe_t: 44, safe_b: 19 },
  ];
  
  class NandosMain {
      init() {
          let pixelDensity = window.devicePixelRatio ? window.devicePixelRatio : 1;
          let isPortrait = screen.orientation && (screen.orientation.angle == 90 || screen.orientation.angle == 270) ? false : true;
          let screenWidth = (isPortrait ? window.screen.width : window.screen.height) * pixelDensity;
          let screenHeight = (isPortrait ? window.screen.height : window.screen.width) * pixelDensity;
  
          document.querySelectorAll("[nandos=blackbar]").forEach(a => a.style.height = "20px");
  
          iosDevices.forEach(x => {
              if(x.safe_t && x.width === screenWidth && x.height === screenHeight) {
                  document.querySelectorAll("[nandos=safe_t]").forEach(a => a.style.height = x.safe_t + "px");
              }
              if(x.safe_b && x.width === screenWidth && x.height === screenHeight) {
                  document.querySelectorAll("[nandos=safe_b]").forEach(a => a.style.paddingBottom = 10 + x.safe_b + "px");
              }
          })
          
      }
  }
  
  class NandosRota {
      _rotaDays = [];
  
      // START OF ROTA
      init() {
          this._rotaDays = this.getWeek();
          this.renderRota();
          document.querySelectorAll("[nandos=myrota_left]").forEach(x => x.addEventListener("click", () => this.rotaLeft()));
          document.querySelectorAll("[nandos=myrota_right]").forEach(x => x.addEventListener("click", () => this.rotaRight()));
  
          document.querySelectorAll("[nandos=myrota_open]").forEach(x => x.addEventListener("click", () => this.showMyRota()));
          document.querySelectorAll("[nandos=fullrota_open]").forEach(x => x.addEventListener("click", () => this.showFullRota()));
  
          this.generateRota();
      }
  
      showMyRota() {
          document.querySelectorAll("[nandos^=page_]").forEach(x => x.style.display = "none");
          document.querySelectorAll("[nandos=page_myrota]").forEach(x => x.style.display = "flex");
      }
  
      showFullRota() {
          document.querySelectorAll("[nandos^=page_]").forEach(x => x.style.display = "none");
          document.querySelectorAll("[nandos=page_fullrota]").forEach(x => x.style.display = "flex");
      }
  
      renderRota() {
          let outHtml = "<strong>" + this._rotaDays[0].toLocaleDateString("en-GB", { day: 'numeric' }) + "</strong> ";
          outHtml += this._rotaDays[0].toLocaleDateString("en-GB", { month: 'short' });
          outHtml +=  " - ";
          outHtml += "<strong>" + this._rotaDays[6].toLocaleDateString("en-GB", { day: 'numeric' }) + "</strong> ";
          outHtml += this._rotaDays[6].toLocaleDateString("en-GB", { month: 'short' });
  
          document.querySelectorAll("[nandos=myrota_date]").forEach(x => x.innerHTML = outHtml);
      }
  
      rotaLeft() {
        let newDay = new Date(this._rotaDays[0].valueOf());
          this._rotaDays = this.getWeek(new Date(this._rotaDays[0].valueOf() - 259200000));
          this.renderRota();
          this.generateRota();
      }
  
      rotaRight() {
          this._rotaDays = this.getWeek(new Date(this._rotaDays[6].valueOf() + 259200000));
          this.renderRota();
          this.generateRota();
      }
      
      getDayOfWeek(now, offset) {
          now ? now : new Date();
          return new Date(now.getFullYear(), now.getMonth(), (now.getDate()+(now.getDay() > 0 ? 1 - now.getDay() : -6)) + (offset ? offset : 0));
      }
  
      getWeek(start) {
          return [...Array(7).keys()].map(x => this.getDayOfWeek(start ? start : new Date(), x));
      }
  
      generateRotaElement(data) {
          let html = `<div class="rotaentry ${data.completed ? 'completed' : ''}">`;
              html += `<div class="div-block-10">`;
                  html += data.is_today ? `<img src="https://assets.website-files.com/63c876725b2dca8264b968f3/63c8d57564b86c1e5b395ec0_Polygon%202.svg" loading="lazy" alt="" class="todayindicator">` : ``;
                  html += `<div class="text-block-6">${data.bits[0]}</div><div class="text-block-7">${data.bits[1]}</div>`;
              html += `</div>`;
              html += `<div class="div-block-11">`;
                  html += `<div class="text-block-8">Grill</div><div class="text-block-9">${data.time}</div>`;
                  html += data.completed ? `<img src="https://assets.website-files.com/63c876725b2dca8264b968f3/63c8d645f4ed498d2817bdff_greentick.svg" loading="lazy" alt="" class="green-tick">` : ``;
              html += `</div>`;
              html += `<div class="div-block-12"></div>`;
          html += `</div>`;

          let el = document.createElement("div");
          el.innerHTML = html;
          return el;
      }
  
      generateMyRota() {
          let now = new Date();
          let x = cyrb53(JSON.stringify(this._rotaDays.map(x => x.toLocaleString()))) + "";
          let daysWorking = [parseInt(x[1]) > 4, parseInt(x[2]) > 4, parseInt(x[3]) > 4, parseInt(x[4]) > 4, parseInt(x[5]) > 4, parseInt(x[6]) > 4, parseInt(x[7]) > 4];
  
          let randex = 7;
  
          let rotaEntries = [];
          let totalHrs = 0;
  
          for(var i = 0; i < 7; i++) {
              let day = this._rotaDays[i];
  
              if(rotaEntries.length > 3 || !daysWorking[i] || (now.getDay() == day.getDay() && now.getMonth() == day.getMonth()))
                  continue;
  
              randex++;
  
              let entry = { 
                  day: day,
                  bits: [day.toLocaleDateString("en-GB", { weekday: 'short' }).toUpperCase(), day.toLocaleDateString("en-GB", { day: 'numeric' })],
                  time: parseInt(x[randex]) > 4 ? "17:00 - 0:00" : "18:00 - 0:00",
                  hours: parseInt(x[randex]) > 4 ? 7 : 6,
                  completed: day < now,
                  is_today: now.getDay() == day.getDay() && now.getMonth() == day.getMonth()
              };
  
              entry.el = this.generateRotaElement(entry);
              
              rotaEntries.push(entry)
              totalHrs += entry.hours;
          }
  
          let el = document.createElement("div");
          el.innerHTML = `<div class="rotaentry"><div class="div-block-10"></div><div class="rotatotal"><div class="text-block-8">Total rota'd hours</div><div class="text-block-9">${totalHrs}</div></div><div class="div-block-12"></div></div>`;
          let totalHours = el.children[0];
  
          document.querySelector(".rota-entries").innerHTML = "";
          rotaEntries.forEach(x => {
              document.querySelector(".rota-entries").appendChild(x.el);
          });
          document.querySelector(".rota-entries").appendChild(totalHours);
      }
  
  
      generateRotaPage(data) {
          let heading = document.querySelector(".yellow-heading");
          let arrows = [];
  
          heading.querySelectorAll(":scope > *").forEach(x => {
              if(x.classList.contains("slide")) return;
  
              if(x.classList.contains("arrow-container-small"))
                  arrows.push(x);
  
              x.remove();
          });
  
          let finalEls = [];
  
          if(arrows[0]) finalEls.push(arrows[0]);

          let hasAToday = data.map(x => x.is_today).indexOf(true) > -1;
  
          for(let day of data) {
              let el = document.createElement("div");
              el.innerHTML = `<div class="div-block-13 ${!hasAToday || day.is_today ? 'today' : ''}"><div class="text-block-11">${day.bits[0]}</div><div class="text-block-10">${day.bits[1]}</div></div>`;
              el = el.querySelector("div");
              el.addEventListener("click", function() {
                document.querySelectorAll(".yellow-heading .today").forEach(x => x.classList.remove("today"));
                el.classList.add("today");
                  day.onActivate();
              });
              finalEls.push(el);
              hasAToday = true;
          }
  
          if(arrows[1]) finalEls.push(arrows[1]);
  
          finalEls.forEach(x => heading.appendChild(x));
      }
  
      generateRotaSlot(data) {
          let html = `<div class="div-block-14">`;
              html += `<div class="text-block-12">${data.name}</div>`;
              html += `<div class="staff-container">`;
              for(let staff of data.staff) {
                  html += `<div class="staff-box">`;
                  html += `<div class="div-block-15"><img src="https://assets.website-files.com/63c876725b2dca8264b968f3/63c8772b973a44c1c6fbbe4c_src_img_homenandoca.png" loading="lazy" width="60" height="60" alt=""></div>`;
                  html += `<div class="div-block-16"><div class="text-block-14">${staff.name}</div><div class="text-block-15">${data.name}</div><div class="text-block-13">From</div></div>`;
                  html += `<div class="div-block-15"><div class="text-block-13">${staff.time}</div></div>`;
                  html += `</div>`;
              }
              html += `</div>`;
          html += `</div>`;
  
          let el = document.createElement("div");
          el.innerHTML = html;
          return el;
      }
  
      generateFullRota() {
          let names = {
              supervisor: [
                "Jasmine Smith",
                "Millie Horner"
              ],
              coord: [
                "Akiel Hypolite",
                "Jodie Smith"
              ],
              host: [
                "Chiara Lamberti",
                "Abdul Ahmadi",
                "Jemima Fernandez",
                "Chanel Collins",
                "Adam Sianga"
              ],
              griller: [
                "Ahmed Abdalla",
                "Andras Nagy",
                "Joshua Onorena",
                "Rafal Zych"
              ]
          };
  
          let slots = [
              {
                  name: "Supervisor",
                  times: ["12:00 - 21:00", "16:00 - 0:00"]
              },
              {
                  name: "Coord",
                  times: ["10:00 - 17:00", "17:00 - 0:00"]
              },
              {
                  name: "Host",
                  times: ["10:00 - 17:00", "12:00 - 21:00", "17:00 - 0:00"]
              },
              {
                  name: "Griller",
                  times: ["10:00 - 17:00", "12:00 - 20:00", "17:00 - 0:00"]
              }
          ];
  
          let now = new Date();
          let x = cyrb53(JSON.stringify(this._rotaDays.map(x => x.toLocaleString()))) + "";
          let daysWorking = [parseInt(x[1]) > 4, parseInt(x[2]) > 4, parseInt(x[3]) > 4, parseInt(x[4]) > 4, parseInt(x[5]) > 4, parseInt(x[6]) > 4, parseInt(x[7]) > 4];
  
          let randex = 7;
  
          let rotaEntries = [];
  
          for(var i = 0; i < 7; i++) {
              let day = this._rotaDays[i];
              let dayEntry = {
                  day: day,
                  bits: [day.toLocaleDateString("en-GB", { weekday: 'short' }).toUpperCase()[0], day.toLocaleDateString("en-GB", { day: 'numeric' })],
                  completed: day < now,
                  is_today: now.getDay() == day.getDay() && now.getMonth() == day.getMonth(),
                  positions: []
              };
  
              for(let slot of slots) {
                  let slotEntry = {
                      name: slot.name,
                      staff: []
                  };
  
                  let sortedNames = shuffleX(names[slot.name.toLowerCase()], parseInt(x));
                  let si = 0;
  
                  for(let time of slot.times) {
                      slotEntry.staff.push({
                          name: sortedNames[si++],
                          time: time
                      });
                  }
  
                  slotEntry.el = this.generateRotaSlot(slotEntry);
                  dayEntry.positions.push(slotEntry);
              }
  
              dayEntry.onActivate = function() {
                  console.log(dayEntry);
                  document.querySelector(".dayscontainer").innerHTML = "";
                  for(let pos of dayEntry.positions) {
                      document.querySelector(".dayscontainer").appendChild(pos.el);
                  }
              }
  
              if(dayEntry.is_today) {
                  dayEntry.onActivate();
              }
  
              rotaEntries.push(dayEntry);
          }
  
          this.generateRotaPage(rotaEntries);
      }
  
      generateRota() {
          this.generateMyRota();
          this.generateFullRota();
      }
  
      // END OF ROTA
  }
  
  class NandosHome {
      init() {
          document.querySelectorAll("[nandos=home_open]").forEach(x => x.addEventListener("click", () => this.showHome()));
      }
  
      showHome() {
          document.querySelectorAll("[nandos^=page_]").forEach(x => x.style.display = "none");
          document.querySelectorAll("[nandos=page_home]").forEach(x => x.style.display = "flex");
      }
  }
  
  class Nandos {
      _main;
      _rota;
      _home;
  
      init() {
          document.body.style.userSelect = "none";
  
          this._main = new NandosMain();
          this._main.init();
  
          this._rota = new NandosRota();
          this._rota.init();
          this._rota.showMyRota();
  
          this._home = new NandosHome();
          this._home.init();
      }
  }
  
  
  function onLoad() {
      window.nandos = new Nandos();
      window.nandos.init();
  }
  
  document.addEventListener('DOMContentLoaded', onLoad);